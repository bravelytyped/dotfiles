" Load 3rd-party plugins
call plug#begin()
Plug 'bagrat/vim-buffet'
Plug 'danilo-augusto/vim-afterglow'
Plug 'easymotion/vim-easymotion'
Plug 'olical/vim-enmasse'
Plug 'tpope/vim-fugitive'
call plug#end()

" Set color scheme and enable syntax highlighting
colorscheme afterglow
syntax on

" Don't fill the background color (to allow for transparent terminal windows;
" this _needs_ to come after `colorscheme`)
highlight Normal ctermbg=NONE

" Keybindings
noremap <Tab> :bn<CR>
noremap <S-Tab> :bp<CR>

" Identation settings
set expandtab
set shiftwidth=2
set softtabstop=2
set tabstop=8

" Keep buffers loaded even if they aren't attached to a window
set hidden

" Show dangling whitespace
set list
set listchars=tab:→\ \|,trail:•,nbsp:-

" Always save Unix line endings (\n)
set fileformat=unix

" Miscellaneous settings
set nowrap
set number

" Customize filetype plugins (this _needs_ to come before `filetype`)
let g:python_recommended_style = 0

" Turn on filetype plugins
filetype plugin indent on
